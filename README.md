# Challenge CH 6 Binar FSW 30

#### Prerequisite to run the app, please make sure you do these things :
- Install PostgreSQL and the service is running.
- Install `sequelize-cli` globally or simply type `npm install -g sequelize-cli`

### Step-by-step to run the app
1. Clone the repository 
2. Open the directory
3. Type **_npm install_** to install required packages
4. After that, make your own **_.env_** file based on **_.env.example_**
5. Run these **commands** in order:
- `sequelize db:create` : will create new database based on your .env file
- `sequelize db:migrate` : will create new tables based on models that are already defined
- `sequelize db:seed:all` : will populate the database with dummy data
6. You can run `npm start` to run the app
7. Go to `localhost:3000/` you will redirected to login page
8. Fill out your username and password based on `/server/data/users.json` file or use this
```
username: nodejs
password: test123
```
9. You are logged in, now you can test the APIs

## Step-By-Step to tests the APIs
1. After logged in, you will landed on dashboard page that shown the data from `userGames` table.
2. From here, you can try to create a new user, edit existing user/data, or delete existing user by simply clicking the approriate button.
3. Lets say you want to edit user data from ID 1 or go to here : `http://localhost:3000/edit/1`
4. Theres few fields that you can edit, it should looks like this :
```
Username :
Password :

Full Name :
Email :
Date of Birth :

Game :
Score :
Level :
Last Login :
```
5. Every Save / Cancel button respectively affects the data represented.
6. After you click save, you will be redirected to main page and see if the data has changed.
7. Now, if you want to create a new data, click 'Plus' mark on the navbar or go to `http://localhost:3000/create`
8. The page should looks like this:
```
Username :
Password :
Username and Password must be between 5 and 20 Characters
```
9. Username and Password should be longer than 5 and lower than 20 characters and cannot be empty.
10. Click save and you will be redirected to main page and see if the new data has been created.
- Note that if you create a new user, it will also create datas related to it (in this case, User Biodata and User Game History)
11. If you want to delete some existing data, click the cross or `X` mark besides the data
- Note that this action has no alert or confirmation, so after you click it, it will instantly deletes the user data and datas related to user (Biodata and Game History)
12.  Finally, you can click logout button or try something on the dashboard

### Additional Information
- Note that if you encounter some error like `usergame is not defined` or etc it's because the error handler doesn't work properly. I'm sorry for this.
- ``userGames`` table have validation on `Username` and `Password` field : must be between 5 and 20 characters and cannot be empty.
- The data on index page sorted descending based on `updatedAt`
- Try install DBeaver or some DBMS and make the connection to app database to check the schema
- The database schema that represented on ER Diagram will be more or less like this
```
userGames has userId as Primary Key and related to userGameBiodata and userGameHistories

userGames has one userGameBiodata and has many userGameHistories
userGameBiodata and userGameHistories belongs to userGames

```