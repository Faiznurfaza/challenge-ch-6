'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('userGames', [
    {username: 'User 1', password: '123456', createdAt: new Date(), updatedAt: new Date()},
    {username: 'User 2', password: '123457', createdAt: new Date(), updatedAt: new Date()},
    {username: 'User 3', password: '123458', createdAt: new Date(), updatedAt: new Date()},
   ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
