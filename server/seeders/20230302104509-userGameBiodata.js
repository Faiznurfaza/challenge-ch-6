'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('userGameBiodata', [
      {userId: 1, fullName: 'User 1', email: 'user1@mail.com', dateofbirth: new Date(1991, 2, 24), createdAt: new Date(), updatedAt: new Date()},
      {userId: 2, fullName: 'User 2', email: 'user2@mail.com', dateofbirth: new Date(1997, 1, 24), createdAt: new Date(), updatedAt: new Date()},
      {userId: 3, fullName: 'User 3', email: 'user3@mail.com', dateofbirth: new Date(1991, 4, 24), createdAt: new Date(), updatedAt: new Date()}
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
