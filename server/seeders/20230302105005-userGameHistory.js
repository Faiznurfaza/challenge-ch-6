'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('userGameHistories', [
    {userId: '1', game: 'valorant', score: '100', level: '90', lastLogin: new Date(2023, 2, 10), createdAt: new Date(), updatedAt: new Date()},
    {userId: '1', game: 'overwatch', score: '110', level: '5', lastLogin: new Date(2023, 5, 10), createdAt: new Date(), updatedAt: new Date()},
    {userId: '2', game: 'dota 2', score: '120', level: '64', lastLogin: new Date(2023, 6, 10), createdAt: new Date(), updatedAt: new Date()},
    {userId: '2', game: 'counter strike', score: '130', level: '23', lastLogin: new Date(2022, 11, 20), createdAt: new Date(), updatedAt: new Date()},
    {userId: '3', game: 'ragnarok x', score: '140', level: '42', lastLogin: new Date(2023, 5, 20), createdAt: new Date(), updatedAt: new Date()},
    {userId: '3', game: 'luna online', score: '150', level: '55', lastLogin: new Date(2023, 1, 21), createdAt: new Date(), updatedAt: new Date()}
   ])
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
